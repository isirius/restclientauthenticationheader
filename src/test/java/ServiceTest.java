import common.Constants;
import common.enumeration.MethodType;
import org.junit.Assert;
import org.junit.Test;

import java.security.NoSuchAlgorithmException;

/**
 * Created by melis on 05/11/15.
 */
public class ServiceTest {
    @Test
    public void testGetPasswordHash(){
        System.out.println("TEST 1 =========");
        String password="12345";
        String hashedPassword = null;
        try {
            hashedPassword = AuthenticationHeaderUtils.sha256(password.getBytes()).toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        System.out.println("hashedPassword="+hashedPassword);
        Assert.assertNotNull(hashedPassword);

    }

    @Test
    public void testGenerateClientNonce(){
        System.out.println("TEST 2 =========");
        String cnonce = AuthenticationHeaderUtils.generateClientNonce();
        System.out.println("cnonce="+cnonce);
        Assert.assertNotNull(cnonce);

    }

    @Test
    public void testGenerateResponse(){
        System.out.println("TEST 3 =========");

        String username="melis@melis.edu.tr";
        String hashedPassword = "ABC";
        String restVerb = MethodType.POST.toString();
        String uri = "/rest/login";
        String serverNonce = "488E54E5CBA86B7E094B1C8DD6D53602";
        String cnonce = "0a4f113b";
        int nc=1;

        String response = AuthenticationHeaderUtils.calculateResponseValue(username, hashedPassword, Constants.MINDER_REALM, restVerb, uri, serverNonce, cnonce, Integer.toString(nc));

        System.out.println(response);
        Assert.assertNotNull(response);

    }
}
