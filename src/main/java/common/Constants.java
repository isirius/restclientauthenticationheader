package common;

/**
 * Collected constants of Minder's rest web services.
 *
 * @author: Melis Ozgur Cetinkaya Demir
 * @date: 15/10/15.
 */
public final class Constants {
    public static final String MINDER_REALM = "rest@mindertestbed.gov";
    public static final String AUTHORIZATION_DIGEST = "xDigest";

    private Constants(){
        //This prevents even the native class from calling this actor.
        throw new AssertionError();
    }
}
