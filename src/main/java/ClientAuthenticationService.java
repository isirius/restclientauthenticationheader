import common.Constants;
import common.enumeration.MethodType;

import java.security.NoSuchAlgorithmException;

/**
 * This class provides the creation of client side authentication header which authorizes the
 * Client to call Minder's REST services.
 *
 * Login mechanism for a rest client:
 * 1. call rest/login service
 * 2. Minder will return a realm and a generated nonce in the header (tag:WWW-Authenticate) with the response code 401 unauthorized.
 * 3. Client prepares Authenticate header (tag:Authorization) according to the Digest Based Authentication standard and calls doLogin method.
 * 4. Minder returns to Client with the response code 200.
 *
 * After login to system, in each request, the client must provide a new authentication header
 * which is generated with a new cnonce and naturally response values in header with the tag AUTHORIZATION.
 * On the other hand, client uses the same server nonce (aka. nonce).
 *
 * The structure of an AUTHORIZATION header is;
 * Digest username="<username>",realm="rest@mindertestbed.gov",nonce="<128bitHexadecimalServerNonce>",
 * uri="<restUriToCall>",nc="requestCounter",cnonce="<32bitHexadecimalClientNonce>",response="<calculatedDigestByMd5>"
 *
 * A sample AUTHORIZATION tag in header generated by a Client is as in the following:
 * Digest username="tester@minder",                 realm="rest@mindertestbed.gov",
 *    nonce="488E54E5CBA86B7E094B1C8DD6D53602",                 uri="/rest/login",
 *    nc=00000001, cnonce="0a4f113b", response="804455ABF82B76244B914C3AB4F24117"
 *
 *
 * @author: Melis Ozgur Cetinkaya Demir
 * @date: 04/11/15.
 */
public final class ClientAuthenticationService {
    private ClientAuthenticationService(){}

    /**
    * To get the authentication header, use ClientAuthenticationService.getAuthenticationHeader(...)
    * Assign the return String to the request's header's as the value of AUTHORIZATION tag.
    *
    * Server expects a request count nc for a <realm,servernonce> tuple, generated by Server side, in each request.
    * for login method, the nc value is expected as 1. However, after a successfull login, client must save the nc counter
    * and increment it by 1 in every request. Otherwise, server cannot authorize client and returns 401: Unauthorized message.
    */
    public final static String getAuthenticationHeader(String username, String password, String serverNonce, MethodType restVerb, String uri,int nc) throws NoSuchAlgorithmException {
        String hashedPassword = AuthenticationHeaderUtils.sha256(password.getBytes()).toString();
        String cnonce = AuthenticationHeaderUtils.generateClientNonce();
        String response = AuthenticationHeaderUtils.calculateResponseValue(username,hashedPassword, Constants.MINDER_REALM, restVerb.toString(), uri,serverNonce,cnonce,Integer.toString(nc));

        String authHeader = AuthenticationHeaderUtils.prepareAuthenticateHeader(username,Constants.MINDER_REALM,serverNonce,uri,nc,cnonce,response);
        return authHeader;
    }


}
