import common.Constants;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.Random;

/**
 * Created by melis on 04/11/15.
 */
public final class AuthenticationHeaderUtils {
    private AuthenticationHeaderUtils(){}

    protected static String prepareAuthenticateHeader(String username, String realm, String serverNonce, String uri,int nc, String cnonce,String response) {
        StringBuilder sb = new StringBuilder();
        sb.append(Constants.AUTHORIZATION_DIGEST);

        sb.append(" ");

        sb.append("username=\"");
        sb.append(username);
        sb.append("\"");

        sb.append(",");
        sb.append("realm=\"");
        sb.append(realm);
        sb.append("\"");

        sb.append(",");
        sb.append("nonce=\"");
        sb.append(serverNonce);
        sb.append("\"");

        sb.append(",");
        sb.append("uri=\"");
        sb.append(serverNonce);
        sb.append("\"");

        sb.append(",");
        sb.append("nc=\"");
        sb.append(String.valueOf(nc));
        sb.append("\"");

        sb.append(",");
        sb.append("cnonce=\"");
        sb.append(cnonce);
        sb.append("\"");

        sb.append(",");
        sb.append("response=\"");
        sb.append(response);
        sb.append("\"");

        return sb.toString();

    }

    protected static String generateClientNonce(){
        Random ranGen = new SecureRandom();
        byte[] aesKey = new byte[4]; // 4 bytes = 32 bits
        ranGen.setSeed(System.currentTimeMillis());
        ranGen.nextBytes(aesKey);

        return convertToHex(aesKey);
    }

    protected static String calculateResponseValue(String userName, String password,String realm, String restVerb, String uri,String nonce,String cnonce,String nc) throws SecurityException{
        /*
        * Calculate HA1 = MD5("<userName>:<realm>:<password>")
        * */
        StringBuilder sb = new StringBuilder();
        sb.append(userName);
        sb.append(":");
        sb.append(realm);
        sb.append(":");
        sb.append(password);

        byte[] hashHA1 = new byte[0];
        try {
            hashHA1 = md5(sb.toString().getBytes());
        } catch (NoSuchAlgorithmException e) {
            throw new SecurityException("Hash algorithm is not defined."+e.getCause());
        }
        String HA1 = convertToHex(hashHA1);
        System.out.println("HA1=" + HA1);


        /*
        * Calculate HA2 = MD5("<verb>:<URi>")
        * */
        StringBuilder sb2 = new StringBuilder();
        sb2.append(restVerb);
        sb2.append(":");
        sb2.append(uri);
        byte[] hashHA2 = new byte[0];
        try {
            hashHA2 = md5(sb2.toString().getBytes());
        } catch (NoSuchAlgorithmException e) {
            throw new SecurityException("Hash algorithm is not defined."+e.getCause());
        }
        String HA2 = convertToHex(hashHA2);
        System.out.println("HA2="+HA2);

        /*
        * Calculate Response = MD5("HA1:\NONCE:\NC:CNONCE:\HA2)
        * */
        StringBuilder sb3 = new StringBuilder();
        sb3.append(HA1);
        sb3.append(":\\");
        sb3.append(nonce);
        sb3.append(":\\");
        sb3.append(nc);
        sb3.append(":");
        sb3.append(cnonce);
        sb3.append(":\\");
        sb3.append(HA2);
        byte[] hashHA3 = new byte[0];
        try {
            hashHA3 = md5(sb3.toString().getBytes());
        } catch (NoSuchAlgorithmException e) {
            throw new SecurityException("Hash algorithm is not defined."+e.getCause());
        }
        System.out.println("HA3="+convertToHex(hashHA3));
        return convertToHex(hashHA3);

    }

    protected static byte[] sha256(byte[] array) throws NoSuchAlgorithmException{
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        return md.digest(array);
    }

    private static String convertToHex(byte[] bytes){
        StringBuilder sb = new StringBuilder();
        for(byte b : bytes){
            sb.append(String.format("%02X", b));
        }
        return sb.toString();
    }


    private static byte[] md5(byte[] array) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        return md.digest(array);
    }

}
